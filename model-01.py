import pandas as pd
import numpy as np


def load_data(path):
    df = pd.read_csv(path,sep=";")
    return df

def preprocessing(df_original):
    df = df_original.loc[df_original['class'].str.contains('Iris-setosa|Iris-virginica')]
    df = df.drop(columns=["petal length","sepal width"])
    df["class"].loc[df["class"] == "Iris-setosa"] = 0
    df["class"].loc[df["class"] == "Iris-virginica"] = 1
    return df

def setXY(df):
    X = df.values
    y = df["class"].values
    return X,y

def main():
    print("Hello World! Diego")
    df_original = load_data("irisdata.csv")
    df = preprocessing(df_original)
    X,y = setXY(df)
    phi = np.zeros((len(X),3))

    #Mapeamos cada datos de S con la función phi (1,x1,x2)
    for n,i,j in zip(range(len(X)),X[:,0],X[:,1]):
        phi[n] = [1,i,j]

    phit = np.transpose(phi)
    S = np.matmul(phit,phi)
    
    Sinv = np.linalg.inv(S)
    yphi = np.zeros(3)
    yphi.shape
    for n in range(len(y)):
        yphi = yphi + phit[:,n] * y[n]
    
    w = np.matmul(Sinv,yphi)

    print(w)
    


if __name__ == "__main__":
    main()




